package org.rapidpm.java.part2.chap01.item02;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 02.03.13
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {

    private final int oid;
    private final String valueA;
    private final String valueB;
    private final String valueC;
    private final String valueD;
    private final String valueE;
    private final String valueF;


    public static class Builder {
        private final int oid;   //Pflichtattribut
        private String valueA; //optionsl
        private String valueB; //optionsl
        private String valueC; //optionsl
        private String valueD; //optionsl
        private String valueE; //optionsl
        private String valueF; //optionsl

        public Builder(int oid) {
            this.oid = oid;
        }


        public Builder addA(final String value) {
            this.valueA = value;
            return this;
        }

        public Builder addB(final String value) {
            this.valueB = value;
            return this;
        }

        public Builder addC(final String value) {
            this.valueC = value;
            return this;
        }

        public Builder addD(final String value) {
            this.valueD = value;
            return this;
        }

        public Builder addE(final String value) {
            this.valueE = value;
            return this;
        }

        public Builder addF(final String value) {
            this.valueF = value;
            return this;
        }

        public DemoKlasse build() {
            return new DemoKlasse(this);
        }

    }

    private DemoKlasse(final Builder b) {
        this.valueA = b.valueA;
        this.valueB = b.valueB;
        this.valueC = b.valueC;
        this.valueD = b.valueD;
        this.valueE = b.valueE;
        this.valueF = b.valueF;

        this.oid = b.oid;
    }


    public static void main(String[] args) {
        final DemoKlasse builder = new Builder(1).addA("X").addE("K").build();

    }


}
