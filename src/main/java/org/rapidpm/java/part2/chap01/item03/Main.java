package org.rapidpm.java.part2.chap01.item03;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 02.03.13
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {

        final DemoEnum[] demoEnums = DemoEnum.values();
        for (DemoEnum demoEnum : demoEnums) {
            System.out.println("demoEnum = " + demoEnum);
            System.out.println("demoEnum.ordinal() = " + demoEnum.ordinal());
        }


        System.out.println(DemoSingelton.Wert01);

    }
}
