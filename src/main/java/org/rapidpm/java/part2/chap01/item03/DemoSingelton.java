package org.rapidpm.java.part2.chap01.item03;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 02.03.13
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
public class DemoSingelton {
    public static final DemoSingelton Wert01 = new DemoSingelton(1);
    public static final DemoSingelton Wert02 = new DemoSingelton(1);

    private int id;

    public int getId() {
        return id;
    }


    private DemoSingelton(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DemoSingelton{" +
                "id=" + id +
                '}';
    }
}
