package org.rapidpm.java.part2.chap01.item01;

import java.util.HashMap;
import java.util.Map;

/**
 * Demo eines Serviceproviders mit static factories
 * User: svenruppert
 * Date: 02.03.13
 * Time: 16:07
 */
public class DemoServiceProvider {

    private static Map<String, Service> serviceMap = new HashMap<>();


    public static final String DEFAULT = "DEFAULT";

    static {
        serviceMap.put(DEFAULT, new Service_A());
        serviceMap.put("A", new Service_A());
        serviceMap.put("B", new Service_B());
    }


    private DemoServiceProvider() {
    }


    public static Service getServiceForName(final String serviceName) {
        if ((serviceName != null) && serviceMap.containsKey(serviceName)) {
            return serviceMap.get(serviceName);
        } else {
            return serviceMap.get(DEFAULT);
        }
    }

    public static Service getService() {
        return serviceMap.get(DEFAULT);
    }

    public void setDefaultService(final Service service) {
        if (service == null) {
            //mache nichts
        } else {
            serviceMap.put(DEFAULT, service);
        }
    }

    public static <String, V extends Service> Map<String,V> newServiceMapInstance(){
        return new HashMap<>();  // ab JDK 7 sonst new HashMap<String, V>();
    }


    public static void main(String[] args) {

        final Map<String, Service> map = DemoServiceProvider.newServiceMapInstance();


    }




}
