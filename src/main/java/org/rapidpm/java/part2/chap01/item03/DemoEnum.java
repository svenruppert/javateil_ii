package org.rapidpm.java.part2.chap01.item03;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 02.03.13
 * Time: 17:50
 * To change this template use File | Settings | File Templates.
 */
public enum DemoEnum {

    WERT_01(1),
    WERT_02(2);

    private int id;

    private DemoEnum(int id) {
        this.id = id;
    }


}
