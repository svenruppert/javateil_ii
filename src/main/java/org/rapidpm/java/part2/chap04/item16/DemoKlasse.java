package org.rapidpm.java.part2.chap04.item16;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {

    public static class ExtendedSet extends HashSet<String> {

        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        @Override
        public boolean add(String string) {
            counter++;
            return super.add(string);
        }

        @Override
        public boolean addAll(Collection<? extends String> c) {
            counter=counter + c.size();
            return super.addAll(c);
        }
    }
   public static class ExtendedList extends ArrayList<String> {

        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        @Override
        public boolean add(String string) {
            counter++;
            return super.add(string);
        }

        @Override
        public boolean addAll(Collection<? extends String> c) {
            counter=counter + c.size();
            return super.addAll(c);
        }
    }


    public static void main(String[] args) {
        final List<String> liste2Add = new ArrayList<>();
        liste2Add.add("Zwei");
        liste2Add.add("Drei");

        final ExtendedList el = new ExtendedList();
        el.add("Eins");
        System.out.println("el.getCounter() = " + el.getCounter());
        el.addAll(liste2Add);
        System.out.println("el.getCounter() = " + el.getCounter());


       final ExtendedSet es = new ExtendedSet();
        es.add("Eins");
        System.out.println("es.getCounter() = " + es.getCounter());

        es.addAll(liste2Add);
        System.out.println("es.getCounter() = " + es.getCounter());


    }



}
