package org.rapidpm.java.part2.chap04.item15;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {

    public static final DemoKlasse ONE = new DemoKlasse(1,1);
    public static final DemoKlasse TWO = new DemoKlasse(2,2);

    private int a;
    private int b;

    public DemoKlasse(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public DemoKlasse addA(final int a){
        return new DemoKlasse(this.a + a, this.b);
    }
    public DemoKlasse addB(final int b){
        return new DemoKlasse(this.a, this.b + b);
    }

    public DemoKlasse valueOf(final int a, final int b){
        return new DemoKlasse(a,b);
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
