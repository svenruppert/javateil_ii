package org.rapidpm.java.part2.chap04.item22;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 18.03.13
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {

    private String demoString = "demoString..";


    public static enum DemoEnum{
        ONE,
        TWO;
    }

    public static interface Formatter{
        public String format();
    }

    private class NonStaticInternalKlasse implements Formatter{
        @Override
        public String format() {
            return "result from internal Implementation";
        }
    }

    public Formatter getFormatter(){
        return this.new NonStaticInternalKlasse();
    }


    public static class StaticDemoSubKlasse{
        private int i;

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        @Override
        public String toString() {
            return "StaticDemoSubKlasse{" +
                    "i=" + i +
                    '}';
        }
    }

    public class NonStaticDemoSubKlasse{
        private int i;

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public String workOnDemoString(){
            return demoString + "work..work..work..";
        }

        @Override
        public String toString() {
            return "NonStaticDemoSubKlasse{" +
                    "i=" + i +
                    '}';
        }
    }

}
