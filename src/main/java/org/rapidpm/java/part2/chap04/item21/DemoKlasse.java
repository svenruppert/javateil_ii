package org.rapidpm.java.part2.chap04.item21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {

    public static interface Sorter<T>{ public void sort(List<T> list2Sort); }

    public static class SorterA implements Sorter<Integer> {

        @Override
        public void sort(List<Integer> list2Sort) {
            Collections.sort(list2Sort, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1.compareTo(o2);
                }
            });
        }
    }

    public static class SorterB implements Sorter<Integer> {

        @Override
        public void sort(List<Integer> list2Sort) {
            Collections.sort(list2Sort, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2.compareTo(o1);
                }
            });
        }
    }

    public static void main(String[] args) {
        final List<Integer> intListe2Sort = new ArrayList<>();
        intListe2Sort.add(1);
        intListe2Sort.add(2);
        intListe2Sort.add(3);
        intListe2Sort.add(4);
        intListe2Sort.add(5);

        sortAndPrint(intListe2Sort, new SorterB());
        sortAndPrint(intListe2Sort, new SorterA());


    }

    private static void sortAndPrint(List<Integer> intListe2Sort, Sorter sorterA) {
        sorterA.sort(intListe2Sort);
        for (final Integer integer : intListe2Sort) {
            System.out.println("integer = " + integer);
        }
    }


}
