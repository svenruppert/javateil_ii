package org.rapidpm.java.part2.chap04.item16;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public class KorrekteDemoKlasse {

    public static class ExtendedSet implements Set<String> {

        private final HashSet<String> hashSet = new HashSet<>();
        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        @Override
        public boolean add(String string) {
            counter++;
            return hashSet.add(string);
        }

        public boolean addAll(Collection<? extends String> c) {
            counter=counter + c.size();
            return hashSet.addAll(c);
        }

        @Override
        public Object[] toArray() {
            return hashSet.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return hashSet.toArray(a);
        }

        public boolean containsAll(Collection<?> c) {
            return hashSet.containsAll(c);
        }


        public boolean retainAll(Collection<?> c) {
            return hashSet.retainAll(c);
        }

        public boolean equals(Object o) {
            return hashSet.equals(o);
        }

        public int hashCode() {
            return hashSet.hashCode();
        }

        public boolean removeAll(Collection<?> c) {
            return hashSet.removeAll(c);
        }

        public Iterator<String> iterator() {
            return hashSet.iterator();
        }


        public int size() {
            return hashSet.size();
        }

        public boolean isEmpty() {
            return hashSet.isEmpty();
        }

        public boolean contains(Object o) {
            return hashSet.contains(o);
        }

        public boolean remove(Object o) {
            return hashSet.remove(o);
        }

        public void clear() {
            hashSet.clear();
        }
    }




    public static class ExtendedList extends ArrayList<String> {

        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        @Override
        public boolean add(String string) {
            counter++;
            return super.add(string);
        }

        @Override
        public boolean addAll(Collection<? extends String> c) {
            counter=counter + c.size();
            return super.addAll(c);
        }
    }


    public static void main(String[] args) {
        final List<String> liste2Add = new ArrayList<>();
        liste2Add.add("Zwei");
        liste2Add.add("Drei");

        final ExtendedList el = new ExtendedList();
        el.add("Eins");
        System.out.println("el.getCounter() = " + el.getCounter());
        el.addAll(liste2Add);
        System.out.println("el.getCounter() = " + el.getCounter());


        final ExtendedSet es = new ExtendedSet();
        es.add("Eins");
        System.out.println("es.getCounter() = " + es.getCounter());

        es.addAll(liste2Add);
        System.out.println("es.getCounter() = " + es.getCounter());


    }





}
