package org.rapidpm.java.part2.chap04.item22;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 18.03.13
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[] args) {
        final DemoKlasse demoKlasse = new DemoKlasse();
        final DemoKlasse.NonStaticDemoSubKlasse nonStaticDemoSubKlasse = demoKlasse.new NonStaticDemoSubKlasse();

        final String demoString = nonStaticDemoSubKlasse.workOnDemoString();
        System.out.println("demoString = " + demoString);

        final DemoKlasse.StaticDemoSubKlasse staticDemoSubKlasse = new DemoKlasse.StaticDemoSubKlasse();

        final DemoKlasse.DemoEnum demoEnum = DemoKlasse.DemoEnum.ONE;
        final DemoKlasse.Formatter formatter = demoKlasse.getFormatter();
        final String demoResult = formatter.format();
    }
}
