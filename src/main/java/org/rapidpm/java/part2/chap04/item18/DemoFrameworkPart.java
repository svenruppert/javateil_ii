package org.rapidpm.java.part2.chap04.item18;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 15:11
 * To change this template use File | Settings | File Templates.
 */
public class DemoFrameworkPart {

    final SimpleDateFormat dateFormat = new SimpleDateFormat("YYY-MM-dd");

    public void writeAll(final List<XMLWritable> writables){
        final File target = new File("Save-" + dateFormat.format(new Date()));
        for (final XMLWritable writable : writables) {
            writable.writeToXMLFile(target);
        }
    }


}
