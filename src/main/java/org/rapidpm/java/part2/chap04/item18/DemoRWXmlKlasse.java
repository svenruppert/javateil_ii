package org.rapidpm.java.part2.chap04.item18;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class DemoRWXmlKlasse implements XMLWritable,XMLReadable<DemoRWXmlKlasse>{

    @Override
    public void writeToXMLFile(File target) {
        //write this to a file.....
    }

    @Override
    public DemoRWXmlKlasse readFromXMLFile(File source) {
        //read from a file
        return new DemoRWXmlKlasse();
    }
}
