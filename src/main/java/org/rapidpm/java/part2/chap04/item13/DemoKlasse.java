package org.rapidpm.java.part2.chap04.item13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 12:28
 * To change this template use File | Settings | File Templates.
 */
public class DemoKlasse {


    private final List<String> strListe = new ArrayList<>();

    public List<String> getStrListe() {
        return Collections.unmodifiableList(strListe);
    }
}
