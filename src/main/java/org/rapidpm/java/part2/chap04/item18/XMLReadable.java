package org.rapidpm.java.part2.chap04.item18;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public interface XMLReadable<T> {

    public T readFromXMLFile(final File source);
}
