package org.rapidpm.java.part2.chap02.item08;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 07.03.13
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
public class DemoMainClass {


    private String a;
    private int b;
    private InnerPublicNonStaticClass nonStaticClass;
    private InnerPrivateClass innerPrivateClass;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemoMainClass that = (DemoMainClass) o;

        if (b != that.b) return false;
        if (a != null ? !a.equals(that.a) : that.a != null) return false;
        if (innerPrivateClass != null ? !innerPrivateClass.equals(that.innerPrivateClass) : that.innerPrivateClass != null)
            return false;
        if (nonStaticClass != null ? !nonStaticClass.equals(that.nonStaticClass) : that.nonStaticClass != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + b;
        result = 31 * result + (nonStaticClass != null ? nonStaticClass.hashCode() : 0);
        result = 31 * result + (innerPrivateClass != null ? innerPrivateClass.hashCode() : 0);
        return result;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof DemoMainClass)) return false;
//
//        DemoMainClass that = (DemoMainClass) o;
//
//        if (b != that.b) return false;
//        if (a != null ? !a.equals(that.a) : that.a != null) return false;
//        if (innerPrivateClass != null ? !innerPrivateClass.equals(that.innerPrivateClass) : that.innerPrivateClass != null)
//            return false;
//        if (nonStaticClass != null ? !nonStaticClass.equals(that.nonStaticClass) : that.nonStaticClass != null)
//            return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = a != null ? a.hashCode() : 0;
//        result = 31 * result + b;
//        result = 31 * result + (nonStaticClass != null ? nonStaticClass.hashCode() : 0);
//        result = 31 * result + (innerPrivateClass != null ? innerPrivateClass.hashCode() : 0);
//        return result;
//    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof DemoMainClass)) return false;
//
//        DemoMainClass that = (DemoMainClass) o;
//
//        if (b != that.b) return false;
//        if (!a.equals(that.a)) return false;
//        if (!innerPrivateClass.equals(that.innerPrivateClass)) return false;
//        if (!nonStaticClass.equals(that.nonStaticClass)) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = a.hashCode();
//        result = 31 * result + b;
//        result = 31 * result + nonStaticClass.hashCode();
//        result = 31 * result + innerPrivateClass.hashCode();
//        return result;
//    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public InnerPublicNonStaticClass getNonStaticClass() {
        return nonStaticClass;
    }

    public void setNonStaticClass(InnerPublicNonStaticClass nonStaticClass) {
        this.nonStaticClass = nonStaticClass;
    }

    public InnerPrivateClass getInnerPrivateClass() {
        return innerPrivateClass;
    }

    public void setInnerPrivateClass(InnerPrivateClass innerPrivateClass) {
        this.innerPrivateClass = innerPrivateClass;
    }

    public class InnerPublicNonStaticClass{
        private String a;
        private int b;

        //Annahme das a null sein kann
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InnerPublicNonStaticClass)) return false;

            InnerPublicNonStaticClass that = (InnerPublicNonStaticClass) o;

            if (b != that.b) return false;
            if (a != null ? !a.equals(that.a) : that.a != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = a != null ? a.hashCode() : 0;
            result = 31 * result + b;
            return result;
        }


//Annahme das a nie null ist
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (!(o instanceof InnerPublicNonStaticClass)) return false;
//
//            InnerPublicNonStaticClass that = (InnerPublicNonStaticClass) o;
//
//            if (b != that.b) return false;
//            if (!a.equals(that.a)) return false;
//
//            return true;
//        }
//
//        @Override
//        public int hashCode() {
//            int result = a.hashCode();
//            result = 31 * result + b;
//            return result;
//        }

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }
    }










    private static class InnerPrivateClass{
        private String a;
        private int b;

        private InnerPrivateClass() {
        }

        //wenn man davon ausgeht, dass equals nie aufgerufen wird

        @Override
        public boolean equals(Object obj) {
            throw new AssertionError("Sollte nie aufgerufen werden..");
        }


        //gueltige Implementierung
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (!(o instanceof InnerPrivateClass)) return false;
//
//            InnerPrivateClass that = (InnerPrivateClass) o;
//
//            if (b != that.b) return false;
//            if (!a.equals(that.a)) return false;
//
//            return true;
//        }
//
//        @Override
//        public int hashCode() {
//            int result = a.hashCode();
//            result = 31 * result + b;
//            return result;
//        }

        private String getA() {
            return a;
        }

        private void setA(String a) {
            this.a = a;
        }

        private int getB() {
            return b;
        }

        private void setB(int b) {
            this.b = b;
        }
    }



}
